package com.firestudio.uidesigndemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AssignmentSeven extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_seven);
    }

    public void eight(View view) {
        Intent intent = new Intent(AssignmentSeven.this,AssignmentEight.class);
        startActivity(intent);
    }
}
