package com.firestudio.uidesigndemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AssignmentTwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_two);
    }

    public void next(View view) {
        Intent intent  = new Intent(AssignmentTwo.this,AssignmentFour.class);
        startActivity(intent);
    }
}
