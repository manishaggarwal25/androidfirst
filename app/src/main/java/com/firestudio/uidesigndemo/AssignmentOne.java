package com.firestudio.uidesigndemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AssignmentOne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_one);
    }

    public void sheep(View view) {
        Intent intent  = new Intent(AssignmentOne.this,AssignmentTwo.class);
        startActivity(intent);
    }
}
