package com.firestudio.uidesigndemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class FifthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);
    }

    public void sixassignment(View view) {
        Intent intent = new Intent(FifthActivity.this,AssignmentSix.class);
        startActivity(intent);
    }
}
