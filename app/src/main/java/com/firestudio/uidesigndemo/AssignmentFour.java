package com.firestudio.uidesigndemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AssignmentFour extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_four);
    }

    public void fifthactivity(View view) {
        Intent intent = new Intent(AssignmentFour.this,FifthActivity.class);
          startActivity(intent);
    }
}
